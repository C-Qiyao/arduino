#include <Wire.h>

#include <IR_Thermometer_Sensor_MLX90614.h>

//发射端程序
#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h> 
IR_Thermometer_Sensor_MLX90614 MLX90614 = IR_Thermometer_Sensor_MLX90614();
int value; 
void setup()
{  Mirf.spi = &MirfHardwareSpi; 
Mirf.init();
Mirf.setRADDR((byte *)"ABCDE"); //设置自己的地址（发送端地址），使用5个字符  
Mirf.payload = sizeof(value);  
Mirf.channel = 90;              //设置所用信道  
Mirf.config();
MLX90614.begin();
Serial.begin(9600);
} 
void loop(){ 
  Mirf.setTADDR((byte *)"FGHIJ");           //设置接收端地址  
value =MLX90614.GetObjectTemp_Celsius();                      //0-255的随机数 
Mirf.send((byte *)&value);                //发送指令，发送随机数value  
while(Mirf.isSending())
{Serial.println(value);
}

delay(1);         //直到发送成功，退出循环  
delay(400);
}
