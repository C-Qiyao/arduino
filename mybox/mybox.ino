#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Adafruit_ssd1306syp.h>
#include <Wire.h>
#include "I2Cdev.h"
#include "MPU6050.h"
#define SDA_PIN 14
#define SCL_PIN 12
#define Addr 0x45
int page = 0; //0主页 1温度 2湿度
Adafruit_ssd1306syp display(SDA_PIN, SCL_PIN);
HTTPClient http;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "ntp1.aliyun.com", 3600, 60000); //ntp服务器设置，阿里云
String GetUrl;                                                //苏宁授时系统
String response;                                              //苏宁日期
String charge;                                                //充电状态
String mutestate = "开";
int muteflag = 1; //0表示无静音
ESP8266WebServer esp8266_server(80);
//MPU6050 accelgyro;
//MPU6050 accelgyro(0x69); // <-- use for AD0 high
int16_t ax, ay, az;
int16_t gx, gy, gz;
int dark = 0; //黑暗模式标记
int counter = 0;
int hours;
float voltage = 0, Temp = 0, hum = 0;
int Soc_cont(float volt)
{
  int soc;
  if (volt >= 4.20)
    soc = 100;
  else if (volt >= 4.06)
    soc = 90 + (volt - 4.06) / (4.20 - 4.06) * 10;
  else if (volt >= 3.98)
    soc = 80 + (volt - 3.98) / (4.06 - 3.98) * 10;
  else if (volt >= 3.92)
    soc = 70 + (volt - 3.92) / (3.98 - 3.92) * 10;
  else if (volt >= 3.87)
    soc = 60 + (volt - 3.87) / (3.92 - 3.87) * 10;
  else if (volt >= 3.82)
    soc = 50 + (volt - 3.82) / (3.87 - 3.82) * 10;
  else if (volt >= 3.79)
    soc = 40 + (volt - 3.79) / (3.82 - 3.79) * 10;
  else if (volt >= 3.77)
    soc = 30 + (volt - 3.77) / (3.79 - 3.77) * 10;
  else if (volt >= 3.74)
    soc = 20 + (volt - 3.74) / (3.77 - 3.74) * 10;
  else if (volt >= 3.68)
    soc = 10 + (volt - 3.68) / (3.74 - 3.68) * 10;
  else if (volt >= 3.45)
    soc = 5 + (volt - 3.45) / (3.68 - 3.45) * 10;
  else if (volt >= 3.00)
    soc = 0 + (volt - 3.00) / (3.45 - 3.00) * 5;
  else
    soc = 0;

  return soc;
}
void net_server()
{
  esp8266_server.begin();
  esp8266_server.on("/", handleRoot);
  esp8266_server.on("/wet", wet);
  esp8266_server.on("/temp", temp);
  esp8266_server.on("/mute", mute);
  esp8266_server.on("/bat", home);
  esp8266_server.on("/vot", home);
  esp8266_server.on("/zuhe", zuhe);

  esp8266_server.onNotFound(handleNotFound);
}
void handleRoot()
{
  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta http-equiv=\"Refresh\" content=\"5\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no\" /><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>Project::BOX</title><style>  h2,h1{line-height:1%;}body {  margin: 0;padding: 0;width: 340px;  background: LightCyan}.button { width: 100px; height: 100px;  text-align: center; font-weight: 100; color: darkcyan;  margin: 0 40px 40px 0;  position: relative; border: 6px solid darkcyan; background: LightCyan;  font-size: 20px;  border-radius: 50%;}.top1 { width: 400px; height: 45px; color: #FFF;  border: 1px solid darkcyan; background: darkcyan; font-size: 25px;  border-radius: 0%;}</style></head><body><a href=\"./pin?light1=1\"><button class=\"button top1\">性能不详的盒子</button></a><center style=\"left: 40px; position: relative;\"></br><a href=\"./vot\"><button type=\"button\" class=\"button\" value=\"temp\">电压<span style=\"color: red;font-size: 25px;\">";

  s += (voltage);
  s += "V</span></button></a><a href=\"./bat\"><button type=\"button\" class=\"button\" value=\"humi\">电量<span style=\"color: green;font-size: 25px;\">";
  s += Soc_cont(voltage);
  s += "%</span></button></a><a href=\"./wet\"><button type=\"button\" class=\"button\" value=\"humi\">湿度<span style=\"color: green;font-size: 25px;\">";
  s += (int)hum;
  s += "%</span></button></a><a href=\"./temp\"><button type=\"button\" class=\"button\" value=\"humi\">温度<span style=\"color: green;font-size: 25px;\">";
  s += (int)Temp;
  s += "°C</span></button></a><a href=\"./zuhe\"><button type=\"button\" class=\"button\" value=\"humi\">组合仪表<span style=\"color: green;font-size: 25px;\">";
  s += "</span></button></a><a href=\"./mute\"><button type=\"button\" class=\"button\" value=\"humi\">静音状态<span style=\"color: green;font-size: 25px;\">";
  s += mutestate;
  s += "</span></button></a></br>Power By XiaoLaodi</center></body></html>";
  esp8266_server.send(200, "text/html", s); // NodeMCU将调用此函数。
}
void home()
{

  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta http-equiv=\"Refresh\" content=\"5\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no\" /><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>Project::BOX</title><style>  h2,h1{line-height:1%;}body {  margin: 0;padding: 0;width: 340px;  background: LightCyan}.button { width: 100px; height: 100px;  text-align: center; font-weight: 100; color: darkcyan;  margin: 0 40px 40px 0;  position: relative; border: 6px solid darkcyan; background: LightCyan;  font-size: 20px;  border-radius: 50%;}.top1 { width: 400px; height: 45px; color: #FFF;  border: 1px solid darkcyan; background: darkcyan; font-size: 25px;  border-radius: 0%;}</style></head><body><a href=\"./pin?light1=1\"><button class=\"button top1\">性能不详的盒子</button></a><center style=\"left: 40px; position: relative;\"></br><a href=\"./vot\"><button type=\"button\" class=\"button\" value=\"temp\">电压<span style=\"color: red;font-size: 25px;\">";

  s += (voltage);
  s += "V</span></button></a><a href=\"./bat\"><button type=\"button\" class=\"button\" value=\"humi\">电量<span style=\"color: green;font-size: 25px;\">";
  s += Soc_cont(voltage);
  s += "%</span></button></a><a href=\"./wet\"><button type=\"button\" class=\"button\" value=\"humi\">湿度<span style=\"color: green;font-size: 25px;\">";
  s += (int)hum;
  s += "%</span></button></a><a href=\"./temp\"><button type=\"button\" class=\"button\" value=\"humi\">温度<span style=\"color: green;font-size: 25px;\">";
  s += (int)Temp;
  s += "°C</span></button></a><a href=\"./zuhe\"><button type=\"button\" class=\"button\" value=\"humi\">组合仪表<span style=\"color: green;font-size: 25px;\">";
  s += "</span></button></a><a href=\"./mute\"><button type=\"button\" class=\"button\" value=\"humi\">静音状态<span style=\"color: green;font-size: 25px;\">";
  s += mutestate;
  s += "</span></button></a></br>Power By XiaoLaodi</center></body></html>";
  esp8266_server.send(200, "text/html", s); // NodeMCU将调用此函数。

  page = 0;
  // esp8266_server.send(200, "text/html", getS());
}
void wet()
{

  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta http-equiv=\"Refresh\" content=\"5\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no\" /><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>Project::BOX</title><style>  h2,h1{line-height:1%;}body {  margin: 0;padding: 0;width: 340px;  background: LightCyan}.button { width: 100px; height: 100px;  text-align: center; font-weight: 100; color: darkcyan;  margin: 0 40px 40px 0;  position: relative; border: 6px solid darkcyan; background: LightCyan;  font-size: 20px;  border-radius: 50%;}.top1 { width: 400px; height: 45px; color: #FFF;  border: 1px solid darkcyan; background: darkcyan; font-size: 25px;  border-radius: 0%;}</style></head><body><a href=\"./pin?light1=1\"><button class=\"button top1\">性能不详的盒子</button></a><center style=\"left: 40px; position: relative;\"></br><a href=\"./vot\"><button type=\"button\" class=\"button\" value=\"temp\">电压<span style=\"color: red;font-size: 25px;\">";

  s += (voltage);
  s += "V</span></button></a><a href=\"./bat\"><button type=\"button\" class=\"button\" value=\"humi\">电量<span style=\"color: green;font-size: 25px;\">";
  s += Soc_cont(voltage);
  s += "%</span></button></a><a href=\"./wet\"><button type=\"button\" class=\"button\" value=\"humi\">湿度<span style=\"color: green;font-size: 25px;\">";
  s += (int)hum;
  s += "%</span></button></a><a href=\"./temp\"><button type=\"button\" class=\"button\" value=\"humi\">温度<span style=\"color: green;font-size: 25px;\">";
  s += (int)Temp;
  s += "°C</span></button></a><a href=\"./zuhe\"><button type=\"button\" class=\"button\" value=\"humi\">组合仪表<span style=\"color: green;font-size: 25px;\">";
  s += "</span></button></a><a href=\"./mute\"><button type=\"button\" class=\"button\" value=\"humi\">静音状态<span style=\"color: green;font-size: 25px;\">";
  s += mutestate;
  s += "</span></button></a></br>Power By XiaoLaodi</center></body></html>";
  esp8266_server.send(200, "text/html", s); // NodeMCU将调用此函数。

  page = 1;
  //esp8266_server.send(200, "text/html", getS());
}
void temp()
{
  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta http-equiv=\"Refresh\" content=\"5\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no\" /><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>Project::BOX</title><style>  h2,h1{line-height:1%;}body {  margin: 0;padding: 0;width: 340px;  background: LightCyan}.button { width: 100px; height: 100px;  text-align: center; font-weight: 100; color: darkcyan;  margin: 0 40px 40px 0;  position: relative; border: 6px solid darkcyan; background: LightCyan;  font-size: 20px;  border-radius: 50%;}.top1 { width: 400px; height: 45px; color: #FFF;  border: 1px solid darkcyan; background: darkcyan; font-size: 25px;  border-radius: 0%;}</style></head><body><a href=\"./pin?light1=1\"><button class=\"button top1\">性能不详的盒子</button></a><center style=\"left: 40px; position: relative;\"></br><a href=\"./vot\"><button type=\"button\" class=\"button\" value=\"temp\">电压<span style=\"color: red;font-size: 25px;\">";

  s += (voltage);
  s += "V</span></button></a><a href=\"./bat\"><button type=\"button\" class=\"button\" value=\"humi\">电量<span style=\"color: green;font-size: 25px;\">";
  s += Soc_cont(voltage);
  s += "%</span></button></a><a href=\"./wet\"><button type=\"button\" class=\"button\" value=\"humi\">湿度<span style=\"color: green;font-size: 25px;\">";
  s += (int)hum;
  s += "%</span></button></a><a href=\"./temp\"><button type=\"button\" class=\"button\" value=\"humi\">温度<span style=\"color: green;font-size: 25px;\">";
  s += (int)Temp;
  s += "°C</span></button></a><a href=\"./zuhe\"><button type=\"button\" class=\"button\" value=\"humi\">组合仪表<span style=\"color: green;font-size: 25px;\">";
  s += "</span></button></a><a href=\"./mute\"><button type=\"button\" class=\"button\" value=\"humi\">静音状态<span style=\"color: green;font-size: 25px;\">";
  s += mutestate;
  s += "</span></button></a></br>Power By XiaoLaodi</center></body></html>";
  esp8266_server.send(200, "text/html", s); // NodeMCU将调用此函数。

  page = 2;
  //esp8266_server.send(200, "text/html", getS());
}
void zuhe() //组合仪表
{
  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta http-equiv=\"Refresh\" content=\"5\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no\" /><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>Project::BOX</title><style>  h2,h1{line-height:1%;}body {  margin: 0;padding: 0;width: 340px;  background: LightCyan}.button { width: 100px; height: 100px;  text-align: center; font-weight: 100; color: darkcyan;  margin: 0 40px 40px 0;  position: relative; border: 6px solid darkcyan; background: LightCyan;  font-size: 20px;  border-radius: 50%;}.top1 { width: 400px; height: 45px; color: #FFF;  border: 1px solid darkcyan; background: darkcyan; font-size: 25px;  border-radius: 0%;}</style></head><body><a href=\"./pin?light1=1\"><button class=\"button top1\">性能不详的盒子</button></a><center style=\"left: 40px; position: relative;\"></br><a href=\"./vot\"><button type=\"button\" class=\"button\" value=\"temp\">电压<span style=\"color: red;font-size: 25px;\">";

  s += (voltage);
  s += "V</span></button></a><a href=\"./bat\"><button type=\"button\" class=\"button\" value=\"humi\">电量<span style=\"color: green;font-size: 25px;\">";
  s += Soc_cont(voltage);
  s += "%</span></button></a><a href=\"./wet\"><button type=\"button\" class=\"button\" value=\"humi\">湿度<span style=\"color: green;font-size: 25px;\">";
  s += (int)hum;
  s += "%</span></button></a><a href=\"./temp\"><button type=\"button\" class=\"button\" value=\"humi\">温度<span style=\"color: green;font-size: 25px;\">";
  s += (int)Temp;
  s += "°C</span></button></a><a href=\"./zuhe\"><button type=\"button\" class=\"button\" value=\"humi\">组合仪表<span style=\"color: green;font-size: 25px;\">";
  s += "</span></button></a><a href=\"./mute\"><button type=\"button\" class=\"button\" value=\"humi\">静音状态<span style=\"color: green;font-size: 25px;\">";
  s += mutestate;
  s += "</span></button></a></br>Power By XiaoLaodi</center></body></html>";
  esp8266_server.send(200, "text/html", s); // NodeMCU将调用此函数。

  page = 3;
  //esp8266_server.send(200, "text/html", getS());
}
void mute()
{ muteflag = !muteflag;
  if (muteflag == 1)
    mutestate = "开";
  else
    mutestate = "关";
  beep();
  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta http-equiv=\"Refresh\" content=\"5\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no\" /><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"><title>Project::BOX</title><style>  h2,h1{line-height:1%;}body {  margin: 0;padding: 0;width: 340px;  background: LightCyan}.button { width: 100px; height: 100px;  text-align: center; font-weight: 100; color: darkcyan;  margin: 0 40px 40px 0;  position: relative; border: 6px solid darkcyan; background: LightCyan;  font-size: 20px;  border-radius: 50%;}.top1 { width: 400px; height: 45px; color: #FFF;  border: 1px solid darkcyan; background: darkcyan; font-size: 25px;  border-radius: 0%;}</style></head><body><a href=\"./pin?light1=1\"><button class=\"button top1\">性能不详的盒子</button></a><center style=\"left: 40px; position: relative;\"></br><a href=\"./vot\"><button type=\"button\" class=\"button\" value=\"temp\">电压<span style=\"color: red;font-size: 25px;\">";

  s += (voltage);
  s += "V</span></button></a><a href=\"./bat\"><button type=\"button\" class=\"button\" value=\"humi\">电量<span style=\"color: green;font-size: 25px;\">";
  s += Soc_cont(voltage);
  s += "%</span></button></a><a href=\"./wet\"><button type=\"button\" class=\"button\" value=\"humi\">湿度<span style=\"color: green;font-size: 25px;\">";
  s += (int)hum;
  s += "%</span></button></a><a href=\"./temp\"><button type=\"button\" class=\"button\" value=\"humi\">温度<span style=\"color: green;font-size: 25px;\">";
  s += (int)Temp;
  s += "°C</span></button></a><a href=\"./zuhe\"><button type=\"button\" class=\"button\" value=\"humi\">组合仪表<span style=\"color: green;font-size: 25px;\">";
  s += "</span></button></a><a href=\"./mute\"><button type=\"button\" class=\"button\" value=\"humi\">静音状态<span style=\"color: green;font-size: 25px;\">";
  s += mutestate;
  s += "</span></button></a></br>Power By XiaoLaodi</center></body></html>";
  esp8266_server.send(200, "text/html", s); // NodeMCU将调用此函数。

 
  //esp8266_server.send(200, "text/html", getS());
}
void handleNotFound()
{                                                                           // 当浏览器请求的网络资源无法在服务器找到时，
  esp8266_server.send(404, "text/plain", "404: Not found，找不到网页啦!~"); // NodeMCU将调用此函数。
}
void SHT30get()
{
  if (counter % 20 == 0) //50个周期对电进行采样
  {
    unsigned int data[6];
    Wire.beginTransmission(Addr);
    Wire.write(0x2C); //发送SHT测了命令0x2c06，因为iic一次只能发送八位数据，所以分两次发送
    Wire.write(0x06);
    Wire.endTransmission(); //停止iic
    delay(100);
    Wire.requestFrom(Addr, 6); //请求获取六字节的数据,分别为高八位温度，低八位温度，校验，高，低八位湿度，校验数据
    if (Wire.available() == 6)
    {
      data[0] = Wire.read();
      data[1] = Wire.read();
      data[2] = Wire.read();
      data[3] = Wire.read();
      data[4] = Wire.read();
      data[5] = Wire.read();
    }
    Temp = ((data[0] * 256.0 + data[1]) * 175 / 65535.0) - 45;
    hum = (data[3] * 256.0 + data[4]) * 100 / 65535.0;
    Serial.println("temp:");
    Serial.println(hum);
    Serial.println(Temp);
  }
}
void dateget()
{
  int httpCode = http.GET();

  if (httpCode > 0)
  {
    Serial.printf("[HTTP] GET... code: %d\n", httpCode);
    if (httpCode == HTTP_CODE_OK)
    {
      response = http.getString();
      Serial.println(response);
    }
  }

  http.end();
}
void diaplaymodeset()
{
  if ((hours >= 23 || hours <= 6) && dark == 0)
  {
    display.initializedark();
    dark = 1;
  }
  else if ((hours > 6 && hours < 23) && dark == 1)
  {
    display.initialize();
    dark = 0;
  }
}
void counterresst()
{
  if (counter >= 1000)
    counter = 0;
}
void readbatvoltage()
{
  if (counter % 50 == 0) //50个周期对电池电压进行采样
  {
    int bat = analogRead(A0);
    voltage = bat / 1024.00 * 3.20 * 2;
    Serial.print(voltage);
    Serial.println("V");
  }
}
void SHT30init()
{
  Wire.begin();
  delay(300);
}
void SmartConfig()
{
  WiFi.mode(WIFI_STA);
  Serial.println("\r\nWait for Smartconfig...");
  WiFi.beginSmartConfig();
  while (1)
  {
    Serial.print(".");
    delay(500); // wait for a second
    if (WiFi.smartConfigDone())
    {
      Serial.println("SmartConfig Success");
      Serial.printf("SSID:%s\r\n", WiFi.SSID().c_str());
      Serial.printf("PSW:%s\r\n", WiFi.psk().c_str());
      break;
    }
  }
}
bool AutoConfig()

{
  WiFi.begin();
  //如果觉得时间太长可改
  for (int i = 0; i < 10; i++)
  {
    int wstatus = WiFi.status();
    if (wstatus == WL_CONNECTED)
    {

      Serial.println("WIFI SmartConfig Success");
      Serial.printf("SSID:%s", WiFi.SSID().c_str());
      Serial.printf(", PSW:%s\r\n", WiFi.psk().c_str());
      Serial.print("LocalIP:");
      Serial.print(WiFi.localIP());
      Serial.print(" ,GateIP:");
      Serial.println(WiFi.gatewayIP());

      return true;
    }
    else
    {
      Serial.print("WIFI AutoConfig Waiting......");
      Serial.println(wstatus);

      display.clear();
      display.setTextSize(1);
      display.setCursor(10, 10);
      display.println("WIFI AutoConfig......");
      display.println(wstatus);
      display.update();
      delay(1000);
    }
  }
  Serial.println("WIFI AutoConfig Faild!");
  display.println("WIFI AutoConfig Faild!");
  delay(1000);
  display.update();
  display.clear();
  display.setTextSize(2);
  display.setCursor(10, 10);
  display.println("EspTouch");
  display.update();
  delay(500);
  return false;
}
String formateday(int day)
{
  switch (day)
  {
  case 0:
    return "SUN";
    break;
  case 1:
    return "MON";
    break;
  case 2:
    return "TUE";
    break;
  case 3:
    return "WED";
    break;
  case 4:
    return "THU";
    break;
  case 5:
    return "FRI";
    break;
  case 6:
    return "SAT";
    break;
  }
}
void timedisplay()
{
  display.clear();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(5, 0);
  display.print("IP:");
  display.setCursor(25, 0);
  display.print(WiFi.localIP());

  display.setTextSize(2);
  display.setCursor(10, 15);
  display.print(response.substring(13, 23));
  display.setCursor(20, 35);
  display.print(timeClient.getFormattedTime());
  display.setTextSize(1);

  display.setCursor(5, 57);
  display.print("SOC:");
  display.print(Soc_cont(voltage));
  display.print("%");
  display.print("      ");
  display.print("DAY:");
  display.println(formateday(timeClient.getDay()));

  display.update();
}
void sleepdisplay()
{
  display.clear();
  display.setTextColor(WHITE);
  display.setTextSize(2);
  display.setCursor(10, 20);
  display.print("Time To");
  display.setCursor(30, 40);
  display.print("Sleep~~");
  display.update();
}
void beep()
{
  if (muteflag == 0)
  {
    digitalWrite(0, HIGH);
    delay(200);
    digitalWrite(0, LOW);
  }
}
void setup()
{
  pinMode(0, OUTPUT); //beep
  pinMode(2, INPUT_PULLUP);
  pinMode(16, OUTPUT);
  digitalWrite(0, LOW);
  display.initialize();
  dark = 0;
  Serial.begin(115200);
  delay(100);

  display.clear();
  display.setTextSize(2);
  display.setCursor(17, 10);
  display.println("Pro Chen");
  display.setTextSize(1);
  display.setCursor(20, 40);
  display.println("Project::BOX V2.0");
  display.update();
  delay(3000);
  delay(1000);

  display.clear();
  display.setTextSize(1);
  display.setCursor(10, 10);
  display.println("Initializing I2C devices...");
  display.update();
  delay(1000);
  display.println("SHT30 connection successful");
  display.update();
  delay(1000);
  if (!AutoConfig())
  {

    display.clear();
    display.setTextSize(1);
    display.setCursor(10, 10);
    display.println("SmartConfig.......");
    display.update();
    SmartConfig();
  }

  display.clear();
  display.setTextSize(1);
  display.setCursor(10, 10);
  display.println("WIFI Config Success");
  display.printf("  SSID:%s\r\n", WiFi.SSID().c_str());
  display.println("  LocalIP:");
  display.print("  ");
  display.println(WiFi.localIP());
  display.println("  GateIP:");
  display.print("  ");
  display.println(WiFi.gatewayIP());
  display.update();
  delay(8000);

  display.clear();
  display.setTextSize(2);
  display.setCursor(10, 10);
  display.println("CONNECTED");
  display.update();
  delay(1000);

  GetUrl = "http://quan.suning.com/getSysTime.do";

  http.setTimeout(5000);

  http.begin(GetUrl);
  net_server();
  SHT30init();
}
void loop()
{

  if (digitalRead(2) == 0)
  {
    beep();
    display.clear();
    display.setTextSize(2);
    display.setCursor(10, 10);
    display.println("Net Reset");
    display.update();
    SmartConfig();
  }

  //accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  //Serial.print(ay);
  //Serial.print("\t");
  /*
  int charg = digitalRead(0);
  if (charg)
    charge = "Charging";
  else
    charge = "";
    */
  SHT30get();
  esp8266_server.handleClient();
  readbatvoltage();
  counterresst();
  timeClient.update();
  hours = timeClient.getHours() + 7;
  diaplaymodeset();
  if (timeClient.zhengHours() == 0)
  {
    if (muteflag == 0)
    {
      beep();
      delay(200);
      beep();
    }
  }

  if (page == 0)
  {
    if (counter == 0)
      dateget();
    //Serial.println(hours);

    if (hours == 0 && timeClient.zhengHours() == 0)
    {
      for (int i = 0; i <= 10; i++)
      {
        timedisplay();
        delay(200);
        sleepdisplay();
        delay(200);
      }
    }
    else if (timeClient.zhengHours() == 0)
    {
      for (int i = 0; i <= 10; i++)
      {
        if (i <= 2)
          timedisplay();
        delay(200);
        display.clear();
        display.update();
        delay(200);
      }
    }
    else
      timedisplay();

    delay(200);
  }

  if (page == 1) //湿度表
  {
    display.clear();
    display.setTextSize(2);
    display.drawCircle(35, 30, 30, WHITE);
    display.setTextSize(1);
    display.setCursor(24, 17);
    display.print("Hum:");
    display.setTextSize(2);
    display.setCursor(20, 28);
    display.print((int)hum);
    display.print("%");
    display.setCursor(75, 14);
    if ((int)hum > 60)
      display.print("WET");
    if ((int)hum < 50)
      display.print("DRY");
    if ((int)hum >= 50 && (int)hum <= 60)
      display.print("GOOD");
    display.setCursor(75, 37);
    display.setTextSize(1);
    display.print("best hum");
    display.setCursor(69, 47);
    display.print("50\% to 60\%");
    display.update();

    delay(200);
  }

  if (page == 2) //温度
  {
    display.clear();
    display.setTextSize(2);
    display.drawCircle(35, 30, 30, WHITE);
    display.setTextSize(1);
    display.setCursor(24, 17);
    display.print("Temp:");
    display.setTextSize(2);
    display.setCursor(25, 28);
    display.print((int)Temp);
    //display.print("'C");
    display.setCursor(75, 14);
    if ((int)Temp > 24)
      display.print("HOT");
    if ((int)Temp < 18)
      display.print("COLD");
    if ((int)Temp >= 18 && (int)hum <= 24)
      display.print("GOOD");
    display.setCursor(75, 37);
    display.setTextSize(1);
    display.print("best temp");
    display.setCursor(69, 47);
    display.print("18 to 24");
    display.update();

    delay(200);
  }

  if (page == 3) //组合表
  {
    display.clear();
    display.setTextSize(2);
    display.drawCircle(34, 30, 30, WHITE);
    display.setTextSize(1);
    display.setCursor(24, 17);
    display.print("Hum:");
    display.setTextSize(2);
    display.setCursor(20, 28);
    display.print((int)hum);
    display.print("%");

    display.setTextSize(2);
    display.drawCircle(98, 30, 30, WHITE);
    display.setTextSize(1);
    display.setCursor(85, 17);
    display.print("Temp:");
    display.setTextSize(2);
    display.setCursor(87, 28);
    display.print((int)Temp);
    // display.print("'C");
    display.update();

    delay(200);
  }
  counter++;
}
