#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ESP8266WiFi.h>
#include <Adafruit_ssd1306syp.h>
#define SDA_PIN 5
#define SCL_PIN 4
Adafruit_ssd1306syp display(SDA_PIN, SCL_PIN);

WiFiUDP ntpUDP;

NTPClient timeClient(ntpUDP, "ntp1.aliyun.com", 3600, 60000);
void SmartConfig()
{
  WiFi.mode(WIFI_STA);
  Serial.println("\r\nWait for Smartconfig...");
  WiFi.beginSmartConfig();
  while (1)
  {
    Serial.print(".");
    delay(500);                   // wait for a second
    if (WiFi.smartConfigDone())
    {
      Serial.println("SmartConfig Success");
      Serial.printf("SSID:%s\r\n", WiFi.SSID().c_str());
      Serial.printf("PSW:%s\r\n", WiFi.psk().c_str());
      break;
    }
  }
}
bool AutoConfig()
{
  WiFi.begin();
  //如果觉得时间太长可改
  for (int i = 0; i < 10; i++)
  {
    int wstatus = WiFi.status();
    if (wstatus == WL_CONNECTED)
    {
      Serial.println("WIFI SmartConfig Success");
      Serial.printf("SSID:%s", WiFi.SSID().c_str());
      Serial.printf(", PSW:%s\r\n", WiFi.psk().c_str());
      Serial.print("LocalIP:");
      Serial.print(WiFi.localIP());
      Serial.print(" ,GateIP:");
      Serial.println(WiFi.gatewayIP());

      return true;
    }
    else
    {
      Serial.print("WIFI AutoConfig Waiting......");
      Serial.println(wstatus);
      display.update();
      display.clear();
      display.setTextSize(2);
      display.setCursor(10, 10);
      display.println("AutoConfig Waiting");
      delay(1000);
    }
  }
  Serial.println("WIFI AutoConfig Faild!" );
  display.update();
  delay(500);
  display.clear();
  display.setTextSize(2);
  display.setCursor(10, 10);
  display.println("EspTouch");
  return false;
}


void setup() {
  delay(1000);
  display.initialize();
  Serial.begin(115200);
  delay(100);

  display.update();
  delay(500);
  display.clear();
  display.setTextSize(2);
  display.setCursor(10, 10);
  display.println("CONNECTING TO WIFI");
  delay(200);
  if (!AutoConfig())
  {
    display.update();
    display.clear();
    display.setTextSize(2);
    display.setCursor(10, 10);
    display.println("SmartConfig");
    SmartConfig();
  }

  display.update();
  delay(500);
  display.clear();
  display.setTextSize(2);
  display.setCursor(10, 10);
  display.println("CONNECTED");
  delay(800);
}
void loop()
{
  timeClient.update();
  Serial.println(timeClient.getFormattedTime());
  display.update();
  delay(200);
  display.clear();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(5, 0);
  display.print("IP:");
  display.setCursor(25, 0);
  display.println(WiFi.localIP());
  display.setTextSize(2);
  display.setCursor(20, 30);
  display.println(timeClient.getFormattedTime());

}
