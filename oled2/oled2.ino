#include "U8glib.h"

U8GLIB_SH1106_128X64 u8g(U8G_I2C_OPT_NONE);

void draw(void) {
  u8g.setFont(u8g_font_unifont);
  u8g.drawStr( 0, 22, "Chen Test");     //控制屏幕显示一行字母  内容为"helloworld
}

void setup(void) {
  if ( u8g.getMode() == U8G_MODE_R3G3B2 ) {
    u8g.setColorIndex(255);     
  }
  else if ( u8g.getMode() == U8G_MODE_GRAY2BIT ) {
    u8g.setColorIndex(3);         
  }
  else if ( u8g.getMode() == U8G_MODE_BW ) {
    u8g.setColorIndex(1);         
  }
  else if ( u8g.getMode() == U8G_MODE_HICOLOR ) {
    u8g.setHiColorByRGB(255,255,255);
  }
}

void loop(void) {
  u8g.firstPage();  
  do {
    draw();
     u8g.drawLine(0,25,100,25);//绘制一条线
    u8g.drawStr( 0, 40, "cqy OLED");
     u8g.drawLine(0,44,50,44);
   
  u8g.drawStr( 0,58,"PASS");          // clear pixel at (28, 14)
  delay(500);
u8g.clearBuffer(); 
  delay(500);
  } while( u8g.nextPage() );
  delay(50);

  
 


}
